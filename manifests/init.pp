# Install and configure the LLDP daemon.

# To enable LLDP, include the following line somewhere in your manifest:
#
#   include lldpd
#
# If you have a switch which uses Cisco Discovery Protocol (CDP) instead of 
# LLDP, after you `include lldpd`, add the following code:
#
#   class { 'lldpd':
#     enable_cdp => true,
#   }
#
# Alternately, you can set "lldpd::enable_cdp: true" in Hiera.

class lldpd (
  $enable_cdp = false,
) {

  # Bring in the lldpd daemon and configuration files
  case $::osfamily {
    # Debian uses lldpd and a simple defaults file
    'debian': {
      package { 'lldpd': ensure => present }

      file { '/etc/default/lldpd':
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('lldp/lldpd.erb'),
        require => Package[lldpd],
      }
    }

    # RHEL uses lldpad and some configuration commands
    'redhat': {
      exec {
        'lldp-enable':
          command => 'lldptool set-lldp adminStatus=rxtx',
          require => Package[lldpad],
      }

      package { 'lldpad':
        ensure => present,
        notify => Exec[lldp-enable],
      }
    }

    # Other OSes aren't supported right now
    default: {
      fail( 'Your OS is not supported by this package' )
    }
  }

}
